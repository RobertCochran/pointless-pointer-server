#!/usr/bin/env python3

# Pointless Pointer Server for Linux - Copyright 2018 Robert Cochran
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# moverel x y
# mousedown [ 0 | 1 ]
# mouseup [ 0 | 1 ]
# recenter

import subprocess
import socket

def pplang_to_xdotool(my_in):
    in_list = my_in.split(" ")

    mouse_button_to_xdotool = {
        "left": 1,
        "right": 3,
    }

    # No switch! Argh!
    if in_list[0] == "moverel":
        return "mousemove_relative --sync -- {} {}".format(int(in_list[1]), int(in_list[2]))
    elif in_list[0] == "mousedown":
        return "mousedown {}".format(mouse_button_to_xdotool[in_list[1]]);
    elif in_list[0] == "mouseup":
        return "mouseup {}".format(mouse_button_to_xdotool[in_list[1]]);
    elif in_list[0] == "recenter":
        return "mousemove --sync --polar 0 0"
    else:
        print("Ignoring unknown command sequence '{}'".format(my_in));

    return "";

with socket.socket() as host_sock:
    host_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    host_sock.bind(("", 12397))
    host_sock.listen()
    while True:
        client_sock, client_addr = host_sock.accept()
        print("Connection from {}".format(client_addr))
        while True:
            try:
                retval = client_sock.recv(4096)
                if len(retval) == 0: break

                for cmd in retval.decode("utf8").split("\n"):
                    with subprocess.Popen(["xdotool", "-"], stdin=subprocess.PIPE, stdout=subprocess.DEVNULL) as proc:
                        if cmd == "": continue;
                        print(cmd)
                        ppcommand = pplang_to_xdotool(cmd.strip())
                        if ppcommand != "" and proc.returncode == None:
                            proc.communicate(bytearray(ppcommand, "utf8"))
            except socket.error as err:
                # Just try again!
                break
        print("Apparently lost connection with {}; closing socket...".format(client_addr))
        client_sock.close()
