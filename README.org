* Pointless Pointer Server

Server-side programs for a gyroscope-driven mouse pointer Android app written as
my final for an Android app development class. Android-side sources unavailable
because I took lots of sample code from the class's sample apps and I'm unsure
of the licensing restrictions on those parts.
