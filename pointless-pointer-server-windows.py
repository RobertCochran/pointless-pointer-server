#!/usr/bin/env python3

# Pointless Pointer Server for Windows - Copyright 2018 Robert Cochran
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# moverel x y
# mousedown [ 0 | 1 ]
# mouseup [ 0 | 1 ]
# recenter

import socket
import ctypes

# Make some shortcuts
mouse_ev = ctypes.windll.user32.mouse_event

# Some constants for mouse_ev
MOUSE_EV_ABSOLUTE =  0x8000
MOUSE_EV_MOVE =      0x0001
MOUSE_EV_LEFTDOWN =  0x0002
MOUSE_EV_LEFTUP =    0x0004
MOUSE_EV_RIGHTDOWN = 0x0008
MOUSE_EV_RIGHTUP =   0x0010

with socket.socket() as host_sock:
    host_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    host_sock.bind(("", 12397))
    host_sock.listen()
    while True:
        client_sock, client_addr = host_sock.accept()
        print("Connection from {}".format(client_addr))
        while True:
            try:
                retval = client_sock.recv(4096)
                if len(retval) == 0: break

                for cmd in retval.decode("utf8").split("\n"):
                    cmd_words = cmd.strip().split(" ");

                    if cmd_words[0] == "moverel":
                        mouse_ev(MOUSE_EV_MOVE, int(cmd_words[1]), int(cmd_words[2]), 0, 0)
                    elif cmd_words[0] == "mousedown":
                        which = MOUSE_EV_LEFTDOWN if cmd_words[1] == "left" else MOUSE_EV_RIGHTDOWN
                        mouse_ev(which, 0, 0, 0, 0)
                    elif cmd_words[0] == "mouseup":
                        which = MOUSE_EV_LEFTUP if cmd_words[1] == "left" else MOUSE_EV_RIGHTUP
                        mouse_ev(which, 0, 0, 0, 0)
                    elif cmd_words[0] == "recenter":
                        # Apparently Windows maps the absolute screen
                        # to a 16-bit value... don't know, don't want
                        # to know...
                        mouse_ev(MOUSE_EV_MOVE | MOUSE_EV_ABSOLUTE, int(65534 / 2), int(65534 / 2), 0, 0)
                    #else:
                        #print("Ignoring unknown command '{}'".format(cmd))
            except socket.error as err:
                # Just try again!
                break
        print("Apparently lost connection with {}; closing socket...".format(client_addr))
        client_sock.close()
